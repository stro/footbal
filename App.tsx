import React from 'react';
import {MainContainer} from './src/navigation';
import {Provider} from 'react-redux';
import 'react-native-gesture-handler';
import {store} from './src/redux/store';

const App = () => {
  return (
    <Provider store={store}>
      <MainContainer />
    </Provider>
  );
};

export default App;
