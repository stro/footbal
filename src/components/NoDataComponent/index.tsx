import React from 'react';
import {Text} from 'react-native';
import {styles} from './style';
import {NO_DATA} from '../../const/strings';

export const NoDataComponent = () => {
  return <Text style={styles.text}>{NO_DATA}</Text>;
};
