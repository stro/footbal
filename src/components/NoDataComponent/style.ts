import {StyleSheet} from 'react-native';
import {colors} from '../../const/colors';

export const styles = StyleSheet.create({
  text: {
    textAlign: 'center',
    color: colors.error,
    fontSize: 24,
    fontWeight: '500',
  },
});
