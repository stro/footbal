export type HeaderType = {
  title: string;
  firstCol: string;
  secondCol: string;
  thirdCol: string;
};
