import React from 'react';
import {View, Text} from 'react-native';
import {HeaderType} from './type';
import {styles} from './style';

export const HeaderWithTitle = (props: HeaderType) => {
  const {title, firstCol, secondCol, thirdCol} = props;

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <View style={styles.rowContainer}>
        <View style={styles.containerForText}>
          <Text numberOfLines={1} ellipsizeMode={'tail'}>
            {firstCol}
          </Text>
        </View>
        <View style={styles.containerForText}>
          <Text numberOfLines={1} ellipsizeMode={'tail'}>
            {secondCol}
          </Text>
        </View>
        <View style={styles.containerForText}>
          <Text numberOfLines={1} ellipsizeMode={'tail'}>
            {thirdCol}
          </Text>
        </View>
      </View>
    </View>
  );
};
