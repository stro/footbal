import {StyleSheet} from 'react-native';
import {colors} from '../../const/colors';

export const styles = StyleSheet.create({
  container: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: colors.lightGray,
    backgroundColor: colors.white,
  },
  title: {
    color: colors.dark,
    fontSize: 18,
    fontWeight: '500',
  },
  containerForText: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  rowContainer: {
    marginTop: 10,
    flexDirection: 'row',
  },
});
