import {StyleSheet} from 'react-native';
import {colors} from '../../const/colors';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15,
    paddingVertical: 10,
    flexDirection: 'row',
    backgroundColor: colors.white,
  },
  containerForText: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
});
