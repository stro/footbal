import React from 'react';
import {View, Text} from 'react-native';
import {styles} from './style';
import {InfoCardType} from './type';

export const InfoCard = (props: InfoCardType) => {
  const {firstRecord, secondRecord, thirdRecord} = props;

  return (
    <View style={styles.container}>
      <View style={styles.containerForText}>
        <Text numberOfLines={1} ellipsizeMode={'tail'}>
          {firstRecord}
        </Text>
      </View>
      <View style={styles.containerForText}>
        <Text numberOfLines={1} ellipsizeMode={'tail'}>
          {secondRecord}
        </Text>
      </View>
      <View style={styles.containerForText}>
        <Text numberOfLines={1} ellipsizeMode={'tail'}>
          {thirdRecord}
        </Text>
      </View>
    </View>
  );
};
