import {TeamType} from '../../redux/types/teamTypes';

export type TeamItemTypes = {
  onPress: () => void;
  team: TeamType;
};
