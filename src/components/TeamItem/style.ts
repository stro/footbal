import {StyleSheet} from 'react-native';
import {colors} from '../../const/colors';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    resizeMode: 'contain',
    width: 30,
    height: 36,
  },
  containerForInfo: {
    flex: 1,
    paddingHorizontal: 10,
  },
  nameText: {
    fontSize: 20,
    color: colors.dark,
  },
  locationText: {
    fontSize: 16,
    color: colors.darkGray,
  },
  nextArrow: {
    fontSize: 18,
    fontWeight: '200',
    color: colors.dark,
  },
});
