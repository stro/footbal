import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {styles} from './style';
import {TeamItemTypes} from './type';
import {getImageUrl} from '../../utils/getImage';
import {LOCATION} from '../../const/strings';

export const TeamItem = (props: TeamItemTypes) => {
  const {team, onPress} = props;
  const uri = getImageUrl(team.crestUrl);

  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Image source={{uri}} style={styles.logo} />
      <View style={styles.containerForInfo}>
        <Text style={styles.nameText}>{team.name}</Text>
        <Text style={styles.locationText}>{LOCATION}: {team.area.name}</Text>
      </View>
      <Text style={styles.nextArrow}>></Text>
    </TouchableOpacity>
  );
};
