import {
  TeamInfoType,
  StateType as StateTypeForTeamReducer,
} from '../../redux/types/teamInfoReducerType';
import {StateType as UpcomingMatchesReducerStateType} from '../../redux/types/upcomingMatchesReducerType';

export type DetailsScreenComponentType = {
  squad: TeamInfoType['squad'];
  teamName: TeamInfoType['name'];
  upcomingMatches: UpcomingMatchesReducerStateType['matches'];
  teamInfoIsLoading: StateTypeForTeamReducer['loading'];
  upcomingMatchesIsLoading: UpcomingMatchesReducerStateType['loading'];
  getTeamInfoFunction: () => void;
  getUpcomingMatchesFunction: () => void;
};
