import React from 'react';
import {View, Text, FlatList} from 'react-native';
import {HeaderWithTitle} from '../../components/HeaderWithTitle';
import {DetailsScreenComponentType} from './types';
import {styles} from './style';
import {NoDataComponent} from '../../components/NoDataComponent';
import {InfoCard} from '../../components/InfoCard';
import {createDate} from '../../utils/createDate';
import {
  PLAYERS,
  UPCOMING_MATCHES,
  NAME,
  NATIONALITY,
  POSITION,
  AWAY_TEAM,
  HOME_TEAM,
  DATE,
} from '../../const/strings';

export const DetailsScreenComponent = (props: DetailsScreenComponentType) => {
  const {
    squad,
    teamName,
    upcomingMatches,
    teamInfoIsLoading,
    upcomingMatchesIsLoading,
    getTeamInfoFunction,
    getUpcomingMatchesFunction
  } = props;

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{teamName}</Text>
      <View style={styles.halfScreen}>
        <HeaderWithTitle
          title={PLAYERS}
          firstCol={NAME}
          secondCol={NATIONALITY}
          thirdCol={POSITION}
        />
        <FlatList
          onRefresh={getTeamInfoFunction}
          refreshing={teamInfoIsLoading}
          keyExtractor={(item) => `${item.id}`}
          data={squad}
          ItemSeparatorComponent={() => <View style={styles.separator} />}
          renderItem={(data) => {
            const {item} = data;

            return (
              <InfoCard
                firstRecord={item.name}
                secondRecord={item.nationality}
                thirdRecord={item.position}
              />
            );
          }}
          ListEmptyComponent={NoDataComponent}
        />
      </View>
      <View style={styles.halfScreen}>
        <HeaderWithTitle
          title={UPCOMING_MATCHES}
          firstCol={HOME_TEAM}
          secondCol={AWAY_TEAM}
          thirdCol={DATE}
        />
        <FlatList
          onRefresh={getUpcomingMatchesFunction}
          refreshing={upcomingMatchesIsLoading}
          keyExtractor={(item) => `${item.id}`}
          data={upcomingMatches}
          ItemSeparatorComponent={() => <View style={styles.separator} />}
          renderItem={(data) => {
            const {item} = data;

            return (
              <InfoCard
                firstRecord={item.homeTeam?.name || ''}
                secondRecord={item.awayTeam?.name || ''}
                thirdRecord={createDate(item.utcDate)}
              />
            );
          }}
          ListEmptyComponent={NoDataComponent}
        />
      </View>
    </View>
  );
};
