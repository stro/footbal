import {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useRoute} from '@react-navigation/native';
import {DetailsScreenComponent} from './DetailsScreen';
import {
  getSquad,
  getTeamName,
  getLoadingState as loadingStateForTeam,
} from '../../redux/selectors/teamInfoSelectors';
import {
  getUpcomingMatches,
  getLoadingState as loadingStateForUpcomingMatches,
} from '../../redux/selectors/upcomingMatchesSelectors';
import {
  TeamInfoType,
  StateType as StateTypeForTeamReducer,
} from '../../redux/types/teamInfoReducerType';
import {StateType as UpcomingMatchesReducerStateType} from '../../redux/types/upcomingMatchesReducerType';
import {getInfoAboutTeamAction} from '../../redux/actions/teamsActions';
import {getUpcomingMatchesAction} from '../../redux/actions/matchesActions';

export const DetailsScreen = () => {
  const {id} = useRoute().params;
  const dispatch = useDispatch();
  const getTeamInfoFunction = () => dispatch(getInfoAboutTeamAction(id));
  const getUpcomingMatchesFunction = () =>
    dispatch(getUpcomingMatchesAction(id));

  useEffect(() => {
    getTeamInfoFunction();
    getUpcomingMatchesFunction();
  }, []);

  const squad: TeamInfoType['squad'] = useSelector(getSquad);
  const teamName: TeamInfoType['name'] = useSelector(getTeamName);
  const teamInfoIsLoading: StateTypeForTeamReducer['loading'] = useSelector(
    loadingStateForTeam,
  );
  const upcomingMatchesIsLoading: UpcomingMatchesReducerStateType['loading'] = useSelector(
    loadingStateForUpcomingMatches,
  );
  const upcomingMatches: UpcomingMatchesReducerStateType['matches'] = useSelector(
    getUpcomingMatches,
  );

  return DetailsScreenComponent({
    squad,
    teamName,
    upcomingMatches,
    teamInfoIsLoading,
    upcomingMatchesIsLoading,
    getTeamInfoFunction,
    getUpcomingMatchesFunction,
  });
};
