import {StyleSheet} from 'react-native';
import {colors} from '../../const/colors';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  halfScreen: {
    flex: 1,
  },
  title: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.dark,
    paddingVertical: 5,
    backgroundColor: colors.white,
  },
  separator: {
    height: 1,
    backgroundColor: colors.lightGray,
    width: '100%',
  },
});
