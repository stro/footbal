import React from 'react';
import {View, FlatList} from 'react-native';
import {styles} from './style';
import {MainScreenComponentTypes} from './types';
import {TeamItem} from '../../components/TeamItem';
import {NoDataComponent} from '../../components/NoDataComponent';

export const MainScreenComponent = (props: MainScreenComponentTypes) => {
  const {goNext, teams, loadingState, getTeamsFunction} = props;

  return (
    <View style={styles.container}>
      <FlatList
        onRefresh={getTeamsFunction}
        refreshing={loadingState}
        keyExtractor={(item) => `${item.id}`}
        data={teams}
        renderItem={(data) => {
          const {item} = data;
          return <TeamItem onPress={() => goNext(item.id)} team={item} />;
        }}
        ListEmptyComponent={NoDataComponent}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
      />
    </View>
  );
};
