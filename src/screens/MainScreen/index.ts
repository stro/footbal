import {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {MainScreenComponent} from './MainScreen';
import {useNavigation} from '@react-navigation/native';
import {DETAILS_SCREEN} from '../../const/screenNames';
import {
  getTeams as getTeamsSelector,
  getLoadingState,
} from '../../redux/selectors/teamsSelector';
import {StateType} from '../../redux/types/teamsReducerType';
import SplashScreen from 'react-native-splash-screen';
import {getTeamsAction} from '../../redux/actions/teamsActions';

export const MainScreen = () => {
  const dispatch = useDispatch();
  const getTeamsFunction = () => dispatch(getTeamsAction());

  useEffect(() => {
    SplashScreen.hide();
    getTeamsFunction();
  }, []);

  const navigation = useNavigation();
  const goNext = (id: number) => navigation.navigate(DETAILS_SCREEN, {id});

  const teams: StateType['listOfTeams'] = useSelector(getTeamsSelector);
  const loadingState: StateType['loading'] = useSelector(getLoadingState);

  return MainScreenComponent({goNext, teams, loadingState, getTeamsFunction});
};
