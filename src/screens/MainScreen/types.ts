import {StateType} from '../../redux/types/teamsReducerType';

export type MainScreenComponentTypes = {
  goNext: (id: number) => void;
  teams: StateType['listOfTeams'];
  loadingState: StateType['loading'];
  getTeamsFunction: () => void;
};
