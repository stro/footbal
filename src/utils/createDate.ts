import moment from 'moment';

export const createDate = (date: string) => {
  const newDate = moment(date).format('DD/MM/YYYY');

  if (newDate === 'Invalid date') {
    return '--/--/--';
  }

  return newDate;
};
