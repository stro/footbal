export const getImageUrl = (url: string | null | undefined): string => {
  if (url && url.indexOf('.svg') === -1) {
    return url;
  }

  return 'https://i7.pngguru.com/preview/860/214/388/football-sport-computer-icons-clip-art-ball.jpg';
};
