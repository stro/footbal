import {StoreType} from '../reducers';
import {StateType} from '../types/upcomingMatchesReducerType';

export const getUpcomingMatches = (state: StoreType): StateType['matches'] => {
  return state.upcomingMatches.matches;
};

export const getLoadingState = (state: StoreType): StateType['loading'] => {
  return state.upcomingMatches.loading;
};
