import {StoreType} from '../reducers';
import {TeamInfoType, StateType} from '../types/teamInfoReducerType';

export const getSquad = (state: StoreType): TeamInfoType['squad'] => {
  // @ts-ignore
  return state.teamInfo.teamInfo?.squad || [];
};

export const getTeamName = (state: StoreType): TeamInfoType['name'] => {
  // @ts-ignore
  return state.teamInfo.teamInfo?.name || '';
};

export const getLoadingState = (state: StoreType): StateType['loading'] => {
  return state.teamInfo.loading;
};
