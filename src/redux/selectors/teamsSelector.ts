import {TeamType} from '../types/teamTypes';
import {StoreType} from '../reducers';
import {StateType} from '../types/teamsReducerType';

export const getTeams = (state: StoreType): TeamType[] => {
  return state.teams.listOfTeams;
};

export const getLoadingState = (state: StoreType): StateType['loading'] => {
  return state.teams.loading;
};
