import {call, put} from 'redux-saga/effects';
import {
  getTeamsSuccessAction,
  getTeamsErrorAction,
  getInfoAboutTeamActionSuccess,
  getInfoAboutTeamActionError,
} from '../actions/teamsActions';
// @ts-ignore
import {getTeams, getTeam} from '@stro1996/js-api-calls';
import {GetInfoWithIdType} from './types';
import {Alert} from 'react-native';
import {TRY_AGAIN} from '../../const/strings';

export function* getTeamsSaga() {
  try {
    const data = yield call(getTeams);
    yield put(getTeamsSuccessAction(data));
  } catch (error) {
    yield put(getTeamsErrorAction(error));
    Alert.alert(error, TRY_AGAIN);
  }
}

export function* getInfoAboutTeam(action: GetInfoWithIdType) {
  try {
    const {id} = action.payload;
    const data = yield call(getTeam, id);
    yield put(getInfoAboutTeamActionSuccess(data));
  } catch (error) {
    yield put(getInfoAboutTeamActionError(error));
    Alert.alert(error, TRY_AGAIN);
  }
}
