import {all, takeLatest} from 'redux-saga/effects';

import {getTeamsSaga, getInfoAboutTeam} from './teamsSagas';
import {getUpcomingMatchesSaga} from './matchesSagas';

import {
  GET_TEAMS,
  GET_INFO_ABOUT_TEAM,
  GET_UPCOMING_MATCHES_FOR_TEAM,
} from '../const/actions';

export const rootSaga = function* () {
  yield all([
    takeLatest(GET_TEAMS, getTeamsSaga),
    takeLatest(GET_INFO_ABOUT_TEAM, getInfoAboutTeam),
    takeLatest(GET_UPCOMING_MATCHES_FOR_TEAM, getUpcomingMatchesSaga),
  ]);
};
