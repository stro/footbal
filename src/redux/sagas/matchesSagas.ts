import {Alert} from 'react-native';
import {call, put} from 'redux-saga/effects';
import {GetInfoWithIdType} from './types';
// @ts-ignore
import {getUpcomingMatches} from '@stro1996/js-api-calls';
import {
  getUpcomingMatchesActionSuccess,
  getUpcomingMatchesActionError,
} from '../actions/matchesActions';
import {TRY_AGAIN} from '../../const/strings';

export function* getUpcomingMatchesSaga(action: GetInfoWithIdType) {
  try {
    const {id} = action.payload;
    const data = yield call(getUpcomingMatches, id);
    yield put(getUpcomingMatchesActionSuccess(data));
  } catch (error) {
    yield put(getUpcomingMatchesActionError(error));
    Alert.alert(error, TRY_AGAIN);
  }
}
