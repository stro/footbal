export type GetInfoWithIdType = {
  type: string;
  payload: {
    id: number;
  };
};
