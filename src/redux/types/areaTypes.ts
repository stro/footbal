export type AreaType = {
  id: number;
  name: string;
};

export type CompetitionArea = {
  code: string;
  ensignUrl: string;
  name: string;
};
