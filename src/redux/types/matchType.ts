import {SeasionType} from './sessionType';
import {ScoreType} from './scoreType';
import {OddsType} from './oddsType';
import {CompetitionType} from './competitionType';
import {ShortTeamType} from './teamTypes';
import {RefereeType} from './refereeType';

export type MatchType = {
  awayTeam: ShortTeamType;
  competition: CompetitionType;
  group: string;
  homeTeam: ShortTeamType;
  id: number;
  lastUpdated: string;
  matchday: number;
  odds: OddsType;
  referees: RefereeType[];
  score: ScoreType;
  season: SeasionType;
  stage: string;
  status: string;
  utcDate: string;
};
