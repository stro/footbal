import {TeamType} from './teamTypes';

export type StateType = {
  listOfTeams: TeamType[];
  loading: boolean;
  error: string;
};
