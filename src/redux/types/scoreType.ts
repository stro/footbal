export type ScoreType = {
  duration: string;
  extraTime: TimeObjType;
  fullTime: TimeObjType;
  halfTime: TimeObjType;
  penalties: TimeObjType;
  winner: null | string;
};

export type TimeObjType = {
  homeTeam: null | number;
  awayTeam: null | number;
};
