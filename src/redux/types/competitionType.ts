import {CompetitionArea} from './areaTypes';

export type CompetitionType = {
  id: number;
  name: string;
  area: CompetitionArea;
};
