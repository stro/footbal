import {TeamType} from './teamTypes';
import {PlayerType} from './playerType';

export type StateType = {
  teamInfo: TeamInfoType | {};
  loading: boolean;
  error: string;
};

export type TeamInfoType = TeamType & {
  squad: PlayerType[];
  lastUpdated: string;
};
