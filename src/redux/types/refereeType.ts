export type RefereeType = {
  id: number;
  name: string;
  nationality: null | string;
};
