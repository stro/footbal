import {MatchType} from './matchType';

export type StateType = {
  matches: MatchType[];
  loading: boolean;
  error: string;
};
