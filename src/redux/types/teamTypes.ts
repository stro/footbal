import {AreaType} from './areaTypes';

export type TeamType = {
  address: string;
  area: AreaType;
  clubColors: string;
  crestUrl?: string;
  email: string | null;
  founded: number;
  id: number;
  lastUpdated: string;
  name: string;
  phone: string;
  shortName: string;
  tla: string;
  venue: string | null;
  website: string;
};

export type ShortTeamType = {
  id: number;
  name: string;
};
