import {
  GET_UPCOMING_MATCHES_FOR_TEAM,
  GET_UPCOMING_MATCHES_FOR_TEAM_SUCCESS,
  GET_UPCOMING_MATCHES_FOR_TEAM_ERROR,
} from '../const/actions';
import {MatchType} from '../types/matchType';

export const getUpcomingMatchesAction = (id: number) => {
  return {
    type: GET_UPCOMING_MATCHES_FOR_TEAM,
    payload: {id},
  };
};

export const getUpcomingMatchesActionSuccess = (data: MatchType[]) => {
  return {
    type: GET_UPCOMING_MATCHES_FOR_TEAM_SUCCESS,
    payload: {data},
  };
};

export const getUpcomingMatchesActionError = (error: string) => {
  return {
    type: GET_UPCOMING_MATCHES_FOR_TEAM_ERROR,
    payload: {error},
  };
};
