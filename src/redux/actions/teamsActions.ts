import {
  GET_TEAMS,
  GET_TEAMS_SUCCESS,
  GET_TEAMS_ERROR,
  GET_INFO_ABOUT_TEAM,
  GET_INFO_ABOUT_TEAM_SUCCESS,
  GET_INFO_ABOUT_TEAM_ERROR,
} from '../const/actions';
import {TeamType} from '../types/teamTypes';

export const getTeamsAction = () => {
  return {
    type: GET_TEAMS,
  };
};

export const getTeamsSuccessAction = (data: TeamType[]) => {
  return {
    type: GET_TEAMS_SUCCESS,
    payload: {data},
  };
};

export const getTeamsErrorAction = (error: string) => {
  return {
    type: GET_TEAMS_ERROR,
    payload: {error},
  };
};

export const getInfoAboutTeamAction = (id: number) => {
  return {
    type: GET_INFO_ABOUT_TEAM,
    payload: {id},
  };
};

export const getInfoAboutTeamActionSuccess = (data) => {
  return {
    type: GET_INFO_ABOUT_TEAM_SUCCESS,
    payload: {data},
  };
};

export const getInfoAboutTeamActionError = (error: string) => {
  return {
    type: GET_INFO_ABOUT_TEAM_ERROR,
    payload: {error},
  };
};
