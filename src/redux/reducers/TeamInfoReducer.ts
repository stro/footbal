import {
  GET_INFO_ABOUT_TEAM,
  GET_INFO_ABOUT_TEAM_SUCCESS,
  GET_INFO_ABOUT_TEAM_ERROR,
} from '../const/actions';
import {StateType} from '../types/teamInfoReducerType';

const initialState: StateType = {
  teamInfo: {},
  loading: true,
  error: '',
};

export const teamInfoReducer = (
  state: StateType = initialState,
  action: any,
) => {
  const {type, payload} = action;

  switch (type) {
    case GET_INFO_ABOUT_TEAM:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case GET_INFO_ABOUT_TEAM_SUCCESS: {
      return {
        ...state,
        loading: false,
        teamInfo: {...payload.data},
      };
    }
    case GET_INFO_ABOUT_TEAM_ERROR: {
      return {
        ...state,
        loading: false,
        error: payload.error,
      };
    }
    default: {
      return state;
    }
  }
};
