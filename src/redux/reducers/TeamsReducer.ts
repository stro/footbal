import {StateType} from '../types/teamsReducerType';
import {GET_TEAMS, GET_TEAMS_SUCCESS, GET_TEAMS_ERROR} from '../const/actions';

const initialState: StateType = {
  listOfTeams: [],
  loading: false,
  error: '',
};

export const teamsReducer = (state: StateType = initialState, action: any) => {
  const {type, payload} = action;

  switch (type) {
    case GET_TEAMS: {
      return {
        ...state,
        loading: true,
        error: '',
      };
    }
    case GET_TEAMS_SUCCESS: {
      return {
        ...state,
        loading: false,
        listOfTeams: [...payload.data],
      };
    }
    case GET_TEAMS_ERROR: {
      return {
        ...state,
        loading: false,
        error: payload.error,
      };
    }
    default: {
      return state;
    }
  }
};
