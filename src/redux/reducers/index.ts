import {combineReducers} from 'redux';
import {teamsReducer} from './TeamsReducer';
import {teamInfoReducer} from './TeamInfoReducer';
import {upcomingMatchesReducer} from './UpcomingMatchesReducer';

import {StateType as UpcomingMatchesReducerStateType} from '../types/upcomingMatchesReducerType';
import {StateType as TeamsReducerStateType} from '../types/teamsReducerType';
import {StateType as TeamInfoReducerStateType} from '../types/teamInfoReducerType';

export type StoreType = {
  teams: TeamsReducerStateType;
  teamInfo: TeamInfoReducerStateType;
  upcomingMatches: UpcomingMatchesReducerStateType;
};

export const rootReducer = combineReducers({
  teams: teamsReducer,
  teamInfo: teamInfoReducer,
  upcomingMatches: upcomingMatchesReducer,
});
