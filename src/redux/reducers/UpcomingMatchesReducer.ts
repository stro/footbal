import {
  GET_UPCOMING_MATCHES_FOR_TEAM,
  GET_UPCOMING_MATCHES_FOR_TEAM_SUCCESS,
  GET_UPCOMING_MATCHES_FOR_TEAM_ERROR,
} from '../const/actions';
import {StateType} from '../types/upcomingMatchesReducerType';

const initialState: StateType = {
  matches: [],
  loading: false,
  error: '',
};

export const upcomingMatchesReducer = (
  state: StateType = initialState,
  action: any,
) => {
  const {type, payload} = action;

  switch (type) {
    case GET_UPCOMING_MATCHES_FOR_TEAM: {
      return {
        ...state,
        loading: true,
        error: '',
      };
    }
    case GET_UPCOMING_MATCHES_FOR_TEAM_SUCCESS:
      return {
        ...state,
        loading: false,
        matches: [...payload.data],
      };
    case GET_UPCOMING_MATCHES_FOR_TEAM_ERROR:
      return {
        ...state,
        loading: false,
        error: payload.error,
      };
    default: {
      return state;
    }
  }
};
