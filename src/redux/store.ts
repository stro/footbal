import createSagaMiddleware from 'redux-saga';
import {
  compose as composeWithoutDevtools,
  createStore,
  applyMiddleware,
} from 'redux';
import {rootReducer} from './reducers';
import {composeWithDevTools} from 'redux-devtools-extension';
import {rootSaga} from './sagas';

const sagaMiddleware = createSagaMiddleware();
const compose: typeof composeWithDevTools = __DEV__
  ? composeWithDevTools
  : composeWithoutDevtools;

export const store = createStore(
  rootReducer,
  compose(applyMiddleware(sagaMiddleware)),
);

sagaMiddleware.run(rootSaga);
