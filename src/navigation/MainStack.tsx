import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {MAIN_SCREEN, DETAILS_SCREEN} from '../const/screenNames';
import {HOME, DETAILS} from '../const/strings';
import {MainScreen} from '../screens/MainScreen';
import {DetailsScreen} from '../screens/DetailsScreen';

const MainStackNavigator = createStackNavigator();

export const MainStack = () => {
  return (
    <MainStackNavigator.Navigator>
      <MainStackNavigator.Screen
        name={MAIN_SCREEN}
        component={MainScreen}
        options={{
          title: HOME,
        }}
      />
      <MainStackNavigator.Screen
        name={DETAILS_SCREEN}
        component={DetailsScreen}
        options={{
          title: DETAILS,
        }}
      />
    </MainStackNavigator.Navigator>
  );
};
