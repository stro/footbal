export const HOME = 'HOME';
export const DETAILS = 'DETAILS';
export const NO_DATA = 'No data to display';
export const LOCATION = 'Location';
export const PLAYERS = 'Players';
export const UPCOMING_MATCHES = 'Upcoming matches';

export const NAME = 'Name';
export const NATIONALITY = 'Nationality';
export const POSITION = 'Position';

export const AWAY_TEAM = 'Away team';
export const HOME_TEAM = 'Home Team';
export const DATE = 'Date';

export const TRY_AGAIN = 'Try again';
