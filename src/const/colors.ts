export const colors = {
  error: '#EE1717',
  white: '#fff',
  dark: '#000',
  darkGray: '#565656',
  lightGray: '#d3d3d3',
};
